using System.Collections.Generic;

namespace FirstFit
{
    public class FirstFitSolver : ISolver
    {
        public void Solve(IEnumerable<Host> hosts, IEnumerable<VM> vms)
        {
            foreach (var vm in vms)
            {
                foreach (var host in hosts)
                {
                    if (canFit(host, vm))
                    {
                        host.AssignedVms.Add(vm);
                        vm.Mapped = true;
                        break;
                    }
                }
                //Console.WriteLine($"{vm.Name}");
                //throw new Exception("could not fit vm");
            }
        }

        private bool canFit(Host host, VM vm)
        {
            return
                host.FreeCPUCores >= vm.CPUCores &&
                host.FreeHDD >= vm.HDD &&
                host.FreeRAM >= vm.RAM;
        }
    }
}
