using System.Linq;
using System.Collections.Generic;

namespace FirstFit
{
    public class Host : HostingEnvironment
    {
        public List<VM> AssignedVms { get; set; } = new List<VM>();
        public int FreeCPUCores { get => this.CPUCores - this.AssignedVms.Sum(x => x.CPUCores); }
        public int FreeRAM { get => this.RAM - this.AssignedVms.Sum(x => x.RAM); }
        public int FreeHDD { get => this.HDD - this.AssignedVms.Sum(x => x.HDD); }
    }
}
