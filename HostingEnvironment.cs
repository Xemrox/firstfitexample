namespace FirstFit
{
    public abstract class HostingEnvironment
    {
        public string Name { get; set; }

        public int CPUCores { get; set; }
        public int RAM { get; set; }
        public int HDD { get; set; }
    }
}
