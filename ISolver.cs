using System.Collections.Generic;

namespace FirstFit
{
    public interface ISolver
    {
        void Solve(IEnumerable<Host> hosts, IEnumerable<VM> vms);
    }
}
