﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace FirstFit
{
    public partial class Programm
    {

        public static void Main(string[] args)
        {
            var hosts = new List<Host>() {
                new Host() {Name = "HM1", HDD = 600, CPUCores = 8, RAM = 20},
                new Host() {Name = "HM2", HDD = 100, CPUCores = 8, RAM = 10},
                new Host() {Name = "HM3", HDD = 200, CPUCores = 4, RAM = 20},
            };
            var vms = new List<VM>() {
                new VM() { Name = "Database1", HDD = 500, CPUCores = 4, RAM = 4 },
                new VM() { Name = "Database2", HDD = 150, CPUCores = 6, RAM = 10 },
                new VM() { Name = "Database3", HDD = 150, CPUCores = 6, RAM = 10 },
                new VM() { Name = "Redis1", HDD = 100, CPUCores = 1, RAM = 20 },
                new VM() { Name = "Redis2", HDD = 0, CPUCores = 2, RAM = 8 },
                new VM() { Name = "Web1", HDD = 10, CPUCores = 4, RAM = 1 },
                new VM() { Name = "Web2", HDD = 50, CPUCores = 4, RAM = 3 },
                new VM() { Name = "Web3", HDD = 5, CPUCores = 1, RAM = 1 }
            };

            var solver = new FirstFitSolver();

            solver.Solve(hosts, vms);

            foreach(var host in hosts) {
                Console.WriteLine(host.Name);
                foreach(var vm in host.AssignedVms) {
                    Console.WriteLine($"\t{vm.Name}");
                }
            }

            foreach(var vm in vms) {
                if(!vm.Mapped)
                    Console.WriteLine($"Not mapped: {vm.Name}");
            }
        }
    }
}
